defmodule Support.LixParserCase do
  @moduledoc ~S"""
  Test Support
  """

  defmacro __using__(_opts) do
    quote do
      use ExUnit.Case
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
