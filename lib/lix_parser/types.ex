defmodule LixParser.Types do
  @moduledoc ~S"""
  Types used throughout this appliaction
  """

  defmacro __using__(_opts) do
    quote do
      @type ast_leaf_t ::
              {:id, position_t(), binaries()}
              | {:literal, scalar_t()}
      @type ast_t ::
              ast_leaf_t()
              | {:list | :map | :quoted, position_t(), list(ast_t())}

      @type binaries :: list(binary())

      @type position_t :: {pos_integer(), pos_integer()}

      @type result_t :: {:error, binary()} | {:ok, ast_t()}

      @type scalar_t :: atom() | binary() | number()
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
