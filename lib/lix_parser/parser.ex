
defmodule LixParser.Parser do
  @moduledoc ~S"""
  Implementation of parse
  """

  use LixParser.Types

  import Minipeg.Combinators
  alias Minipeg.Parsers, as: P

  @spec parse(binary()) :: result_t()
  def parse(string) do
    Minipeg.Parser.parse_string(parser(), string)
  end

  defp atom_parser() do
    P.rgx_capture_parser(":([[:alpha:]][[:word:]]*)", "atom_parser") 
    |> map(&{:lit, String.to_atom(&1)})
  end

  defp float_parser() do
    P.rgx_match_parser("[-+]?\\d+\\.\\d+", "float_parser") 
    |> map(fn parsed ->
      {value, ""} = Float.parse(parsed)
      {:lit, value}
    end)
  end

  defp id_parser() do
    first_char_parser = select([P.char_parser("_"), P.char_parser(:alpha)])
    list_parser(P.ident_parser("id_parser", first_char_parser: first_char_parser), P.char_parser("."), "id_parser", 1) 
    |> mapp(fn elements, pos -> {:id, pos, elements} end)
  end

  defp integer_parser() do
    P.rgx_match_parser("[-+]?\\d+", "integer°parser") 
    |> map(&{:lit, String.to_integer(&1)})
  end

  defp op_parser() do
    P.rgx_match_parser("[-+=/*&@^%#]{1,3}(?![-+=/*&@^%#])")
    |> mapp(fn match, pos -> {:id, pos, [match]} end)
  end

  defp parser() do
    select([
      token(scalar_parser()),
      lazy(fn -> token(sexp_parser()) end)
    ])
  end

  defp scalar_parser() do
    select([
      float_parser(),
      integer_parser(),
      string_parser(),
      atom_parser(),
      id_parser(),
      op_parser()
    ])
  end

  defp sexp_list_parser(open, close, type) do
    sep_parser  = P.ws_parser(true, 1, "seperator_parser")
    inner_parser = list_parser(parser(), sep_parser)
    sequence([
      token(P.char_parser(open)),
      token(inner_parser),
      token(P.char_parser(close)),
    ])
    |> mapp(fn [_, content, _], pos -> {type, pos, content} end, "sexp_list_parser")
  end

  def sexp_map_parser() do
    sequence([
      token(P.char_parser("{")),
      many_seq([
        token(parser()),
        token(parser()),
      ]),
      token(P.char_parser("}")),
    ])
    |> mapp(fn [_, pairs, _], pos -> {:map, pos, pairs |> List.flatten} end, "sexp_map_parser")
  end

  defp sexp_parser() do
    select([
      sexp_list_parser("(", ")", :list),
      sexp_list_parser("[", "]", :quoted),
      sexp_map_parser(),
    ])
  end

  defp string_parser() do
    P.delimited_string_parser(~s{"})
    |>map(fn content ->
      {:lit, IO.chardata_to_string(content)}
    end)
  end

  defp token(parser) do
    sequence([
      ignore(many(P.char_parser(" \t\n"))),
      parser,
    ])
    |> map(&List.first/1)
  end
end
# SPDX-License-Identifier: Apache-2.0
